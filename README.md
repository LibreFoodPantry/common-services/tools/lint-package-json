# Lint package.json

Checks for errors in package.json files.

This is a thin wrapper around
[npm-package-json-lint](https://npmpackagejsonlint.org/).

## Using in the Pipeline

To enable Lint package.json in your project's pipeline add
`lint-package-json` to the `ENABLE_JOBS`
variable in your project's `.gitlab-ci.yml` e.g.:

```bash
variables:
  ENABLE_JOBS: "lint-package-json"
```

The `ENABLE_JOBS` variable is a comma-separated string value, e.g.
`ENABLE_JOBS="job1,job2,job3"`

## Using in Visual Studio Code

There is no Visual Studio Code extension for linting `package.json` files.

You should use the instructions for running locally below.

## Using Locally in Your Development Environment

Lint package.json can be run locally in a linux-based bash shell or in a
linting shell script with this Docker command:

```bash
docker run -v "${PWD}:/workdir" -w /workdir
  registry.gitlab.com/librefoodpantry/common-services/tools/linters/lint-package-json:latest
  npmPkgJsonLint -c .npmpackagejsonlintrc.json .
```

## Configuration

Each project should have a `.npmpackagejsonlintrc.json` file at the top level
of the project which is used for
[npmpackagejsonlint configuration](https://npmpackagejsonlint.org/docs).

There is an example `.npmpackagejsonlintrc.json`
file in this project.

## Licensing

* Code is licensed under [GPL 3.0](documentation/licenses/gpl-3.0.txt).
* Content is licensed under
  [CC-BY-SA 4.0](documentation/licenses/cc-by-sa-4.0.md).
* Developers sign-off on [DCO 1.0](documentation/licenses/dco.txt)
  when they make contributions.
