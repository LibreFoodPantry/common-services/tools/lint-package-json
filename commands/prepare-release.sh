#!/usr/bin/env bash

set -e

mkdir -p ./artifacts/release
echo "# Build ${1}" >> ./artifacts/release/lint-package-json.yml
cat ./source/gitlab-ci/lint-package-json.yml >> ./artifacts/release/lint-package-json.yml
